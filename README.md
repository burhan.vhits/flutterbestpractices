
# Flutter Best Practices

This README describes the Best Practices should be used for Flutter App Development.


## Required Tools

- Android Studio Chipmunk (2021.2.1) or Later
- XCode Version 13.4.1 (13F100) or Later

### Development Components Overview
| Description|Component Details|
|:------------:|:-----------------:|
|Programming Language|Dart 2.18.2 or later|
|Architectural Pattern|BLoC|
|State Management|BLoC , Provider, GetX|
|Navigation Component|Go Router|
|Network Library|Dio 4.0.6 or Later|
|Version Control System|Gitlab




## Why These ?

### Dart

- Dart is a client-optimized language for developing fast apps on any platform. 
- Its goal is to offer the most productive programming language for multi-platform development, paired with a flexible execution runtime platform for app frameworks.

### Architectural Pattern

BloC  makes it easy to implement the Business Logic Component design pattern, which separates presentation from business logic.

Only the thing that will change is: BLOC will be replaced with ViewModel in MVVM.

This flutter project is made using bloc state management and the folder structure of this project is
as follows.

```

├── lib
|   ├── api
│   │   └── api_exception.dart
|   |   └── api_service.dart
|   |   └── dio_helper.dart
|   ├── blocs
|   |   ├── user_mgmt_bloc
│   │   │   └── user_mgmt_bloc.dart
│   |   |   └── user_mgmt_event.dart
│   |   |   └── user_mgmt_state.dart
|   ├── models
|   |   └── home_model.dart
|   |   └── user_model.dart
|   ├── repository
|   |   └── user_mgmt_repository.dart
|   ├── ui
|   |   ├── screens
│   │   │   └── login_screen.dart
│   │   │   └── splash_screen.dart
|   |   ├── widgets
│   │   │   └── app_bar_widget.dart
│   │   │   └── product_item_widget.dart
|   ├── utils
|   |   ├── constants
|   |   |   └── app_strings.dart
|   |   |   └── app_colors.dart
|   ├── main.dart
├── pubspec.lock
├── pubspec.yaml

```

Now, lets dive into the lib folder which has the main code for the application.

```
1- api - Contains all the api service releated files.
2- blocs - Contains all the Business logic of the project.
3- models - Contains all the models files of project. 
4- repository - Contains all the repository files of project.
5- ui — Contains all the ui code of project.
6- util — Contains the utilities/common functions and constant of your application.
7- main.dart - This is the starting point of the application. All the application level configurations are defined in this file i.e, theme, routes, title, orientation etc.
```


### State Management

State management is a complex topic. It is one of the most important processes in the life cycle of an application. In Flutter State is whatever data you need in order to rebuild your UI at any moment in time. When this data change, it will trigger a redraw of the user interface.

A List of state management approaches we've been using in projects.

- BLoC
- Provider
- GetX


### Navigation Component (Go Router)

- A declarative routing package for Flutter that uses the Router API to provide a convenient, url-based API for navigating between different screens. 
- You can define URL patterns, navigate using a URL, handle deep links, and a number of other navigation-related scenarios.



## Programming Rules

### Naming Convention

We are following the below-naming conventions to achieve Readability, Clarity, Consistency in code.

**Package and File Names**

- Package and File name should be in `lowercase_with_underscores` and self descriptive.


**Class Names**

- Class Name should be in `UpperCamelCase` and self descriptive.

|Do's & Don'ts|❌ ✅|
|:--|:--:|
| “Person_Model”,”_PersonModel” |❌|
| ”PersonModel_”,”Person Model”|❌|
| “personModel”, ”PERSONMODEL”|❌|
| ”personmodel”|❌|
|“PersonModel”, “LoginScreen”|✅|
|”AuthRepository”, "AuthBloc"|✅|

**Variable Names**

- Variable name should be in `lowerCamelCase`.
- Access Specifiers should be maintained properly.
- Private Members should be define with prefix `_` 
- `late`, `dynamic`, `var`, `?:`,`!`,`?` should me maintained properly.

**Things to follow**

- Always use stateless widget whenever it is possible.
- Avoid using set state in whole screen. try to do it with state management or define another widget for that so it only rebuild that particular widget.
- Avoid using methods for screen design make widgets instead of methods.
- Try to refactor your large lines of code into widgets.
- If class contains more then 500 lines of code then try to refactor into methods or widgets.
- Always place your widgets inside `widgets` folder so it can be accessible throughout project.
- Define const whenever it is need to define.
- Follow lint rules.
- Maintain version code and version name for both platforms.
- Always update version code before whenever creating PR or MR for staging branch.
